/**
 * Created by atticus on 04.01.17.
 */
var serverURL = "http://localhost:3100";

function handleErrors(response) {
  if (!response.ok) {
    externalThis.setState({error: error});
    throw Error(response);
  }
  return response;
}

export function sendRequest(externalThis, method, url, payload, responseName = "response", parameterString = "") {
  var myHeader = new Headers();
  myHeader.append("Content-Type", "application/json");

  if (method == "GET") {
    var myInit = {
      method: method,
      headers: myHeader,
      mode: 'cors',
    };
  } else {
    var myInit = {
      method: method,
      headers: myHeader,
      mode: 'cors',
      body: JSON.stringify(payload)
    };
  }

  let paramUrl = serverURL + url + ".json?access_token=" + window.sessionStorage.getItem('token') + parameterString;

  fetch(paramUrl, myInit)
    .then(response => {
      if(response.ok) {
        response.json().then(response => externalThis.setState({[responseName]: response}));
      } else {
        response.json().then(response => externalThis.setState({error: response}));
      }
    })
    .catch(function (error) {
      console.log('There has been a problem with your fetch operation: ', error);
  });
}

export function sendRequestAndPrepareSwitchData(externalThis, method, url, payload, responseName = "response", switchData) {
  var myHeader = new Headers();
  myHeader.append("Content-Type", "application/json");

  if (method == "GET") {
    var myInit = {
      method: method,
      headers: myHeader,
      mode: 'cors',
    };
  } else {
    var myInit = {
      method: method,
      headers: myHeader,
      mode: 'cors',
      body: JSON.stringify(payload)
    };
  }

  let paramUrl = serverURL + url + ".json?access_token=" + window.sessionStorage.getItem('token');

  fetch(paramUrl, myInit)
    .then(
      response => {
        if (!response.ok) {
          throw Error(response);
        }
        return response;
      }
    )
    .then(response => response.json())
    .then((response) => {
        for (let i = 0; i < response.length; i++) {
          let selectString = "";
          for (let j = 0; j < switchData.length; j++){
            selectString = selectString + (response[i])[switchData[j]] + " ";
          }
          response[i]['label'] = selectString;
        }
      return response;
      }
    )
    .then(response => externalThis.setState({[responseName]: response}))
    .catch(function (error) {
      error.json().then(res=>externalThis.setState({error: res}))
      console.log('There has been a problem with your fetch operation: ', error);
    });
}


export function sendLoginRequest(externalThis, url, email, password) {
  let myBody = new FormData();
  myBody.append("grant_type", "password");
  myBody.append("email", email);
  myBody.append("password", password);
  let myHeader = new Headers();
  myHeader.append("Content-Type", "application/json");

  let myInit = {
    method: 'POST',
    body: myBody
  };

  let fullUrl = serverURL + url;

  fetch(fullUrl, myInit)
    .then(response => response.json())
    .then(response => externalThis.setState({response: response}))
    .catch(function (error) {
      console.log('There has been a problem with your fetch operation: ' + error.message);
    });
}
