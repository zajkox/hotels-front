/**
 * Created by atticus on 03.01.17.
 */

import React from 'react';
import {Textfield, Button} from 'react-mdl';
import s from './styles.css';
import {sendLoginRequest} from '../../core/request';
import {resolve} from '../../core/router';

class LoginPage extends React.Component {
  constructor() {
    super();
    this.state = {login: '', password: '', response: ''};
    this.handleClick = this.handleClick.bind(this);
  }

  static propTypes = {
    error: React.PropTypes.object,
  };


  handleClick() {
    sendLoginRequest(this, "/oauth/token", this.state.login, this.state.password);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.response != this.state.response) {
      console.log(this.state.response);
      if(!this.state.response.error){
        window.sessionStorage.setItem("token", this.state.response.access_token);
      }
    }
  }

  render() {
    return (
      <div className={s.container}>
        <main className={s.content}>
          <h1 className={s.title}>Login page</h1>
          <div>
            <Textfield
              onChange={e => {
                this.setState({login: e.target.value})
              }}
              label="Login"
              floatingLabel
              style={{width: '200px'}}
            />
          </div>
          <div>
            <Textfield
              onChange={e => {
                this.setState({password: e.target.value})
              }}
              label="Password"
              floatingLabel
              style={{width: '200px'}}
            />
          </div>
          <div>
            <Button raised onClick={this.handleClick}>Login</Button>
          </div>
        </main>
      </div>
    );
  }
}

export default LoginPage;
