/**
 * Created by atticus on 04.01.17.
 */

import React from 'react';
import {TableHeader, Table, Spinner, Button} from 'react-mdl';
import s from './DataList.css';

class DataList extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  render() {
      var singleObject = this.props.response[0];
      var keys = Object.keys(singleObject);
      var headerArray = [];
      var component = TableHeader;
      for (var i = 0; i < keys.length; i++) {
        headerArray.push(React.createElement(component, {key: keys[i], name: keys[i]}, keys[i]));
      }

      return (
        <div ref={node => (this.root = node)}>
          <div>
            <Table
              className={`mdl-data-table ${s.table}`}
              shadow={0}
              selectable
              onSelectionChanged={(a)=>this.props.onSelection(a)}
              rows={this.props.response}
            >
              {headerArray}
            </Table>
          </div>
        </div>
      );
  }
}

export default DataList;
