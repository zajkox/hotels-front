/**
 * Created by atticus on 02.01.17.
 */

import React, { PropTypes, Link} from 'react';
import UltimateDataList from '../UltimateDataList';
import {Button, Spinner} from 'react-mdl';

const DATA_NOT_DOWNLOADED = "DATA_NOT_DOWNLOADED";

class SpinnerList extends React.Component {

  static propTypes ={
    data: React.PropTypes.any
  };

  constructor(props){
    super(props);
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  render() {
    if(this.props.data == DATA_NOT_DOWNLOADED){
      return(
        <div ref={node => (this.root = node)}>
          <div>
            <Spinner/>
          </div>
        </div>
      );
    } else if (this.props.data.length == 0) {
      return(
        <div ref={node => (this.root = node)}>
          <div>
            RESPONSE EMPTY
          </div>
        </div>
      );
    } else {
      return (
        <div ref={node => (this.root = node)}>
          <div>
            <div>
              <UltimateDataList
                {...this.props}
              />
            </div>
          </div>
        </div>
      )
    }
  }
}

export default SpinnerList
