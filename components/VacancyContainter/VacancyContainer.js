/**
 * Created by atticus on 04.01.17.
 */

import React from 'react';
import {
  Tooltip,
  TableHeader,
  Table,
  Spinner,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemContent,
  ListItemAction,
  Checkbox,
  Grid,
  Cell,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions
} from 'react-mdl';
import UltimateDataAdd from '../UltimateDataAdd';
import SpinnerList from '../SpinnerList';
import {sendRequest} from '../../core/request';
import s from './VacancyContainer.css';
var moment = require('moment');
import Websocket from 'react-websocket';

const DATA_NOT_DOWNLOADED = "DATA_NOT_DOWNLOADED";

class VacancyContainer extends React.Component {
  connection;
  constructor(props) {
    super(props);
    this.getDataToBeUpdated = this.getDataToBeUpdated.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.updateParentState = this.updateParentState.bind(this);
    this.addData = this.addData.bind(this);
    this.deleteWithId = this.deleteWithId.bind(this);
    this.state = {openDialog: false, dataToBeUpdated: "", dataToBeAdded: "", fcData: "", response: ""};
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
    this.connection = new WebSocket('ws://localhost:3100/cable');
    this.connection.onopen = () => {
      console.log("ONOPEN");
      this.connection.send("{\"command\":\"subscribe\",\"identifier\":\"{\\\"channel\\\":\\\"RoomsChannel\\\"}\"}");
    };

    this.connection.onmessage = evt => {
      let msg = JSON.parse(evt.data);
      if(msg["type"] != "ping" && msg["message"]){
        this.setState({fcData: DATA_NOT_DOWNLOADED});
        console.log("NON-PING MESSAGE");
      }
    }
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  prepareParameterString(){
    let parameter = "";
    for (let j = 0; j < this.props.params.length; j++) {
      console.log(this.state.dataToBeAdded[this.props.params[j]]);
      parameter = parameter + "&" + this.props.params[j] + "=" + (this.state.dataToBeAdded[this.props.params[j]]).format().substring(0,10);
    }
    return parameter;
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.fcData == DATA_NOT_DOWNLOADED){
      sendRequest(this, 'GET', this.props.url, {}, "fcData", this.prepareParameterString());
    }
  }

  getDataToBeUpdated(data) {
    this.setState({dataToBeUpdated: data});
    this.setState({openDialog: true});
  }

  deleteWithId(id){
    let fullUrl = this.props.url + "/" + id;
    sendRequest(this, "DELETE", fullUrl);
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  handleCloseDialog() {
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
  }

  updateParentState(mode, payload) {
    if (mode == "ADD") {
      this.setState({dataToBeAdded: payload});
    }
    if (mode == "UPDATE"){
      this.setState({dataToBeUpdated: payload});
    }
  }

  addData(){
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  saveChanges() {
    let pl = {};
    for (let i = 0; i < this.props.apiNames.length; i++) {
      pl[this.props.apiNames[i]] = this.state.dataToBeUpdated[this.props.apiNames[i]];
    }
    let fullUrl = this.props.url + "/" + this.state.dataToBeUpdated["id"];
    sendRequest(this, 'PUT', fullUrl, pl);
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  render() {
    return (
      <div ref={node => (this.root = node)}>
        <Grid>
          <Cell col={4}>
            <div>
              <UltimateDataAdd {...this.props}
                               key="UDA2"
                               mode="ADD"
                               data={this.state.dataToBeAdded}
                               updateParentState={this.updateParentState}
                               addButtonOnClick={this.addData}
              />
            </div>
          </Cell>
          <Cell col={8}>
            <SpinnerList {...this.props}
                         getDataToBeUpdated={this.getDataToBeUpdated}
                         data={this.state.fcData}
                         onDelete={this.deleteWithId}
                         key={"FC" + String(this.props.titleKeys)}
                         editableDataList={false}
            />
          </Cell>
        </Grid>
      </div>
    );
  }
}


export default VacancyContainer;
