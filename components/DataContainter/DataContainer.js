/**
 * Created by atticus on 04.01.17.
 */

import React from 'react';
import {
  Tooltip,
  TableHeader,
  Table,
  Spinner,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemContent,
  ListItemAction,
  Checkbox,
  Grid,
  Cell,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions
} from 'react-mdl';
import UltimateDataAdd from '../UltimateDataAdd';
import SpinnerList from '../SpinnerList';
import {sendRequest} from '../../core/request';
import s from './DataContainer.css';

const DATA_NOT_DOWNLOADED = "DATA_NOT_DOWNLOADED";

class DataContainer extends React.Component {
  constructor(props) {
    super(props);
    this.getDataToBeUpdated = this.getDataToBeUpdated.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleCloseErrorDialog = this.handleCloseErrorDialog.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.updateParentState = this.updateParentState.bind(this);
    this.addData = this.addData.bind(this);
    this.deleteWithId = this.deleteWithId.bind(this);
    this.state = {openDialog: false, openErrorDialog: false, dataToBeUpdated: "", dataToBeAdded: "", fcData: DATA_NOT_DOWNLOADED, response: ""};
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
    sendRequest(this, 'GET', this.props.url, "", "fcData");
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.fcData == DATA_NOT_DOWNLOADED) {
      sendRequest(this, 'GET', this.props.url, "", "fcData");
    }

    if (prevState.error != this.state.error){
      this.setState({openErrorDialog: true});

    }
  }

  getDataToBeUpdated(data) {
    this.setState({dataToBeUpdated: data});
    this.setState({openDialog: true});
  }

  deleteWithId(id) {
    let fullUrl = this.props.url + "/" + id;
    sendRequest(this, "DELETE", fullUrl);
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  handleCloseDialog() {
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
  }

  updateParentState(mode, payload) {
    if (mode == "ADD") {
      this.setState({dataToBeAdded: payload});
    }
    if (mode == "UPDATE") {
      this.setState({dataToBeUpdated: payload});
    }
  }

  addData() {
    sendRequest(this, 'POST', this.props.url, this.state.dataToBeAdded);
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  saveChanges() {
    let pl = {};
    for (let i = 0; i < this.props.apiNames.length; i++) {
      pl[this.props.apiNames[i]] = this.state.dataToBeUpdated[this.props.apiNames[i]];
    }
    let fullUrl = this.props.url + "/" + this.state.dataToBeUpdated["id"];
    sendRequest(this, 'PUT', fullUrl, pl);
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  handleCloseErrorDialog() {
    this.setState({openErrorDialog: false});
  }


  render() {
    return (
      <div ref={node => (this.root = node)}>
        <Dialog open={this.state.openDialog} className={s.dialog}>
          <DialogContent>
            <UltimateDataAdd {...this.props}
                             key="UDA1"
                             mode="UPDATE"
                             buttonLabel=""
                             data={this.state.dataToBeUpdated}
                             updateParentState={this.updateParentState}
            />
          </DialogContent>
          <DialogActions>
            <Button type='button' onClick={this.saveChanges}>Save Changes</Button>
            <Button type='button' onClick={this.handleCloseDialog}>Cancel</Button>
          </DialogActions>
        </Dialog>

        <Dialog open={this.state.openErrorDialog} className={s.dialog}>
          <DialogContent>
            There has been an error!
            <br/>
            {JSON.stringify(this.state.error)}
          </DialogContent>
          <DialogActions>
            <Button type='button' onClick={this.handleCloseErrorDialog}>Close</Button>
          </DialogActions>
        </Dialog>

        <Grid>
          <Cell col={4}>
            <div>
              <UltimateDataAdd {...this.props}
                               key="UDA2"
                               mode="ADD"
                               data={this.state.dataToBeAdded}
                               updateParentState={this.updateParentState}
                               addButtonOnClick={this.addData}
              />
            </div>
          </Cell>
          <Cell col={8}>
            <SpinnerList {...this.props}
                         getDataToBeUpdated={this.getDataToBeUpdated}
                         data={this.state.fcData}
                         onDelete={this.deleteWithId}
                         key={"FC" + String(this.props.titleKeys)}
                         editableDataList={true}
            />
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default DataContainer;
