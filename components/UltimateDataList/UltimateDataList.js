/**
 * Created by atticus on 04.01.17.
 */

import React from 'react';
import {
  Tooltip,
  TableHeader,
  Table,
  Spinner,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemContent,
  ListItemAction,
  Checkbox
} from 'react-mdl';
import s from './UltimateDataList.css';
import {sendRequest} from '../../core/request';

class UltimateDataList extends React.Component {

  static propTypes ={
    data: React.PropTypes.any
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  //HAX
  shouldComponentUpdate(){
    return false;
  }

  prepareListItemArray() {
    let singleObject = this.props.data[0];
    let keys = Object.keys(singleObject);
    let listItemArray = [];

    for (var i = 0; i < this.props.data.length; i++) {
      let titleString = "";
      let subtitleString = "";
      let id = this.props.data[i]["id"];
      let currentElement = this.props.data[i];

      for (let j = 0; j < this.props.titleKeys.length; j++) {
        titleString += currentElement[this.props.titleKeys[j]];
        titleString += " ";
      }

      for (let j = 0; j < keys.length; j++) {
        subtitleString = subtitleString + keys[j] + ":" + this.props.data[i][keys[j]] + "  ";
      }

      if(this.props.editableDataList) {
        listItemArray.push(
          React.createElement(ListItem, {key: ("LIA" + i), threeLine: true},
            React.createElement(ListItemContent, {avatar: 'person', subtitle: subtitleString}, titleString),
            React.createElement(ListItemAction, {},
              React.createElement(Tooltip, {label: "Delete", position: "left"},
                React.createElement(IconButton, {name: "delete", onClick: ( () => this.props.onDelete(id))}, "Button")
              ),
              React.createElement(Tooltip, {label: "Edit", position: "left"},
                React.createElement(IconButton, {
                  name: "edit",
                  onClick: ( () => this.props.getDataToBeUpdated(currentElement))
                }, "Button")
              )
            )
          )
        );
      } else {
        listItemArray.push(
          React.createElement(ListItem, {key: ("LIA" + i), threeLine: true},
            React.createElement(ListItemContent, {avatar: 'person', subtitle: subtitleString}, titleString),
          )
        );
      }
    }
    return listItemArray;
  }

  render() {
    let listItemArray = this.prepareListItemArray();

    return (
      <div ref={node => (this.root = node)} className={s.main}>
        <div>
          <List>
            {listItemArray}
          </List>
        </div>
      </div>
    );

  }
}

export default UltimateDataList;
