/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes} from 'react';
import Link from '../Link';
import cx from 'classnames';
import Footer from '../Footer';
import s from './Layout.css';
import {Layout as L, Content, Header, HeaderRow, HeaderTabs, Tab, Textfield, Button} from 'react-mdl';
import Switcher from '../Switcher'

class Layout extends React.Component {

  static propTypes = {
    className: PropTypes.string,
  };

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
    this.selectLoginComponent();
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  constructor(props){
    super(props);

    var tabMap ={
      0: "WELCOME",
      1: "RESERVATIONS",
      2: "GUESTS",
      3: "ROOMS",
      4: "VACANT",
    };

    this.state={activeTab: this.props.activeTab, tabMap: tabMap, loginComponent: ''};
  }

  selectLoginComponent(){
    if(!window.sessionStorage.getItem('token')){
      let component = React.createElement(Link, {to: '/login'}, 'Login');
      this.setState({loginComponent: component});
    } else {
      let component = React.createElement('div', {}, 'You are logged in');
      this.setState({loginComponent: component});
    }
  }

  componentDidUpdate(){
    //this.selectLoginComponent();
  }

  removeToken(){
    window.sessionStorage.removeItem('token');
    this.setState({loginComponent: React.createElement(Link, {to: '/login'}, 'Login')});
  }

  render() {
    return (
      <div ref={node => (this.root = node)}>
        <L fixedHeader style={{zIndex : 100001}}>
            <Header>
              <HeaderRow title="Hotel Manager 2016" >
                {this.state.loginComponent}
                <Button onClick={()=>this.removeToken()}>Remove token</Button>
              </HeaderRow>
              <HeaderTabs ripple
                          activeTab={this.state.activeTab}
                          onChange={(tabId) => this.setState({ activeTab: tabId })}>
                <Tab>Welcome</Tab>
                <Tab>Reservations</Tab>
                <Tab>Guests</Tab>
                <Tab>Rooms</Tab>
                <Tab>Find Vacant Rooms</Tab>
              </HeaderTabs>
            </Header>
          <Content className={`${s.content}`}>
              <Switcher {...this.props}
                        activeTabName={this.state.tabMap[this.state.activeTab]}
                        className={cx(s.content, this.props.className)}/>
            </Content>
            <Footer />
        </L>
      </div>
    );
  }
}

export default Layout;
