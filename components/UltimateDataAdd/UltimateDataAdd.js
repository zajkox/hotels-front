/**
 * Created by atticus on 08.01.17.
 */

import React from 'react';
import {Textfield, Button, Tooltip} from 'react-mdl';
import s from './UltimateDataAdd.css';
import DatePicker from 'react-datepicker';
var moment = require('moment');
require('!!style!css!react-datepicker/dist/react-datepicker.css');
import Select from 'react-select';
require('!!style!css!react-select/dist/react-select.css');

const DATA_NOT_DOWNLOADED = "DATA_NOT_DOWNLOADED";

class UltimateDataAdd extends React.Component {
  static propTypes = {
    error: React.PropTypes.object,
    fieldNames: React.PropTypes.array.isRequired,
    apiNames: React.PropTypes.array.isRequired,
    label: React.PropTypes.string.isRequired,
    url: React.PropTypes.string.isRequired,
    buttonLabel: React.PropTypes.string.isRequired,
    type: React.PropTypes.array.isRequired,
    data: React.PropTypes.any,
    mode: React.PropTypes.string.isRequired,
    updateParentState: React.PropTypes.func,
    updateParentSelectState: React.PropTypes.func,
    addButtonOnClick: React.PropTypes.func,
    sfData: React.PropTypes.any
  };

  constructor(props) {
    super(props);
    if (this.props.fieldNames.length != this.props.apiNames.length) {
      console.error("fieldNames and apiNames length mismatch");
    }
    this.prepareAndUpdateParentTextfield = this.prepareAndUpdateParentTextfield.bind(this);
    this.prepareAndUpdateParentDatePicker = this.prepareAndUpdateParentDatePicker.bind(this);
    this.prepareAndUpdateParentSelectField = this.prepareAndUpdateParentSelectField.bind(this);
  }

  componentWillMount(){
    switch (this.props.mode) {
      case "ADD": {
        let payload = {};
        for (let i = 0; i < this.props.apiNames.length; i++) {
          payload[this.props.apiNames[i]] = undefined;
          if (this.props.type[i] == 'df') {
            payload[this.props.apiNames[i]] = moment();
          }
        }
        this.props.updateParentState(this.props.mode, payload);
        break;
      }
      case "UPDATE": {
        let payload = {};
        for (let i = 0; i < this.props.apiNames.length; i++) {
          payload[this.props.apiNames[i]] = this.props.data[this.props.apiNames[i]];
          if (this.props.type[i] == 'df') {
            payload[this.props.apiNames[i]] = moment(this.props.data[this.props.apiNames[i]]);
          }
        }
        this.props.updateParentState(this.props.mode, payload);
        break;
      }
    }
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  prepareAndUpdateParentTextfield(apiName, event){
    let data = this.props.data;
    data[apiName] = event.target.value;
    this.props.updateParentState(this.props.mode, data);
    console.log("UDA_PREPARE_PARENT_UPDATE_TEXTFIELD_DATA", data);
  }

  prepareAndUpdateParentDatePicker(apiName, moment){
    let data = this.props.data;
    data[apiName] = moment;
    this.props.updateParentState(this.props.mode, data);
    console.log("UDA_PREPARE_PARENT_UPDATE_DATEPICKER_DATA", data);
  }

  prepareAndUpdateParentSelectField(apiName, event){
    this.props.updateParentSelectState(apiName+"_selected", event);
    console.log("UDA_PREPARE_PARENT_UPDATE_SELECTFIELD_DATA", event);
  }

  prepareComponentArray() {
    let componentArray = [];
    let sfDataCounter = 0;
    switch (this.props.mode) {
      case "ADD":
        for (let i = 0; i < this.props.fieldNames.length; i++) {
          let apiName = this.props.apiNames[i];
          switch (this.props.type[i]) {
            case 'tf': {
              componentArray.push(
                React.createElement('div', {key: (this.props.mode + i)},
                  React.createElement(Textfield, {
                    key: (this.props.mode + this.props.fieldNames[i]),
                    label: this.props.fieldNames[i],
                    floatingLabel: true,
                    onChange: (
                      e => {
                        this.prepareAndUpdateParentTextfield(apiName, e)
                      }
                    )
                  })
                )
              );
              break;
            }
            case 'df': {
              componentArray.push(
                React.createElement('div', {key: (this.props.mode + i)},
                  React.createElement(Tooltip, {label: this.props.fieldNames[i]},
                    React.createElement(DatePicker, {
                      key: (this.props.mode + this.props.fieldNames[i]),
                      className: s.calendar,
                      selected: this.props.data[apiName],
                      onChange: (
                        e => {
                          this.prepareAndUpdateParentDatePicker(apiName, e)
                        }
                      ),
                    })
                  )
                )
              );
              break;
            }
            case 'sf': {
              if(this.props[apiName] != DATA_NOT_DOWNLOADED) {
                componentArray.push(
                  React.createElement('div', {key: (this.props.mode + i)},
                    React.createElement(Tooltip, {label: this.props.fieldNames[i]},
                      React.createElement(Select, {
                        key: (this.props.mode + this.props.fieldNames[i]),
                        selected: this.props.data[apiName],
                        className: s.select,
                        placeholder: this.props.fieldNames[i],
                        options: this.props[apiName],
                        value: this.props[apiName+"_selected"],
                        onChange: (
                          e => {
                            this.prepareAndUpdateParentSelectField(apiName, e)
                          }
                        ),
                      })
                    )
                  )
                );
                sfDataCounter++;
                break;
              }
            }
          }
        }
        break;
      case "UPDATE":
        for (let i = 0; i < this.props.fieldNames.length; i++) {
          let apiName = this.props.apiNames[i];
          switch (this.props.type[i]) {
            case 'tf': {
              componentArray.push(
                React.createElement('div', {key: (this.props.mode + i)},
                  React.createElement(Textfield, {
                    key: (this.props.mode + this.props.fieldNames[i]),
                    label: this.props.fieldNames[i],
                    floatingLabel: true,
                    value: this.props.data[apiName],
                    onChange: (
                      e => {
                        this.prepareAndUpdateParentTextfield(apiName, e)
                      }
                    )
                  })
                )
              );
              break;
            }
            case 'df': {
              componentArray.push(
                React.createElement('div', {key: (this.props.mode + i)},
                  React.createElement(Tooltip, {label: this.props.fieldNames[i]},
                    React.createElement(DatePicker, {
                      key: (this.props.mode + this.props.fieldNames[i]),
                      className: s.calendar,
                      selected: moment(this.props.data[apiName]),
                      inline: true,
                      onChange: (
                        e => {
                          this.prepareAndUpdateParentDatePicker(apiName, e)
                        }
                      )
                    })
                  )
                )
              );
              break;
            }
            case 'sf': {
              if(this.props[apiName] != DATA_NOT_DOWNLOADED) {
                componentArray.push(
                  React.createElement('div', {key: (this.props.mode + i)},
                    React.createElement(Tooltip, {label: this.props.fieldNames[i]},
                      React.createElement(Select, {
                        key: (this.props.mode + this.props.fieldNames[i]),
                        selected: this.props.data[apiName],
                        className: s.select,
                        placeholder: this.props.fieldNames[i],
                        options: this.props[apiName],
                        value: this.props[apiName+"_selected"],
                        onChange: (
                          e => {
                            this.prepareAndUpdateParentSelectField(apiName, e)
                          }
                        ),
                      })
                    )
                  )
                );
                sfDataCounter++;
                break;
              }
            }
          }
        }
        break;
    }
    return componentArray;
  }

  render() {
    let componentArray = this.prepareComponentArray();
    if (this.props.buttonLabel != "") {
      return (
        <main className={s.content} ref={node => (this.root = node)}>
          <h1 className={s.title}>{this.props.label}</h1>
          <div>
            <div>
              {componentArray}
            </div>
          </div>

          <br/>

          <div>
            <Button raised onClick={this.props.addButtonOnClick}>{this.props.buttonLabel}</Button>
          </div>
        </main>
      );
    } else {
      return (
        <main className={s.content} ref={node => (this.root = node)}>
          <h1 className={s.title}>{this.props.label}</h1>
          <div>
            <div>
              {componentArray}
            </div>
          </div>
        </main>
      );
    }
  }
}

export default UltimateDataAdd;
