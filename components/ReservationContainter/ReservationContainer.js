/**
 * Created by atticus on 04.01.17.
 */

import React from 'react';
import {
  Tooltip,
  TableHeader,
  Table,
  Spinner,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemContent,
  ListItemAction,
  Checkbox,
  Grid,
  Cell,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions
} from 'react-mdl';
import UltimateDataAdd from '../UltimateDataAdd';
import SpinnerList from '../SpinnerList';
import {sendRequest, sendRequestAndPrepareSwitchData} from '../../core/request';
import s from './ReservationContainer.css';

const DATA_NOT_DOWNLOADED = "DATA_NOT_DOWNLOADED";

class ReservationContainer extends React.Component {
  constructor(props) {
    super(props);
    this.getDataToBeUpdated = this.getDataToBeUpdated.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.updateParentState = this.updateParentState.bind(this);
    this.updateParentSelectState = this.updateParentSelectState.bind(this);
    this.addData = this.addData.bind(this);
    this.deleteWithId = this.deleteWithId.bind(this);
    this.handleCloseErrorDialog = this.handleCloseErrorDialog.bind(this);
    this.state = {openDialog: false, openErrorDialog: false, dataToBeUpdated: "", dataToBeAdded: "", fcData: DATA_NOT_DOWNLOADED, response: ""};


    let j = 0;
    for (let i = 0; i < this.props.type.length; i++) {
      if (this.props.type[i] == 'sf') {
        this.state[this.props.apiNames[i]] = DATA_NOT_DOWNLOADED;
        this.state[this.props.apiNames[i] + "_selected"] = undefined;
        sendRequestAndPrepareSwitchData(this, 'GET', this.props.sfUrl[j], '', this.props.apiNames[i], this.props.sfNameFields[j]);
        j++;
      }
    }
  }

  componentDidMount() {
    window.componentHandler.upgradeElement(this.root);
    sendRequest(this, 'GET', this.props.url, "", "fcData");

    let j = 0;
    for (let i = 0; i < this.props.type.length; i++) {
      if (this.props.type[i] == 'sf') {
        sendRequestAndPrepareSwitchData(this, 'GET', this.props.sfUrl[j], '', this.props.apiNames[i], this.props.sfNameFields[j]);
        j++;
      }
    }
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.root);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.fcData == DATA_NOT_DOWNLOADED) {
      sendRequest(this, 'GET', this.props.url, "", "fcData");
    }

    if (prevState.error != this.state.error){
      this.setState({openErrorDialog: true});

    }
  }

  getDataToBeUpdated(data) {
    for (let i = 0; i < this.props.type.length; i++) {
      if (this.props.type[i] == 'sf') {
        console.log(data);
        console.log("---------", data[this.props.apiNames[i]]);
        console.log("++++++++++", this.state[this.props.apiNames[i]][0]['id']);
        let obj = {};
        for(let j = 0; j < this.state[this.props.apiNames[i]].length; j++){
          if (this.state[this.props.apiNames[i]][j]['id'] == data[this.props.apiNames[i]]){
            obj = this.state[this.props.apiNames[i]][j];
            break;
          }
        }
        console.log("OBJECT", obj);
        this.setState({[this.props.apiNames[i]+"_selected"]: obj});
      }
    }

    console.log("Data",data);
    this.setState({dataToBeUpdated: data});
    console.log(this.state.dataToBeUpdated);
    this.setState({openDialog: true});
  }

  deleteWithId(id) {
    let fullUrl = this.props.url + "/" + id;
    sendRequest(this, "DELETE", fullUrl);
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  handleCloseDialog() {
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
  }

  handleCloseErrorDialog() {
    this.setState({openErrorDialog: false});
  }

  updateParentState(mode, payload) {
    if (mode == "ADD") {
      this.setState({dataToBeAdded: payload});
    }
    if (mode == "UPDATE") {
      this.setState({dataToBeUpdated: payload});
    }
  }

  updateParentSelectState(name, value) {
    this.setState({[name]: value})
  }

  addData() {
    let data = this.state.dataToBeAdded;
    for (let i = 0; i < this.props.apiNames.length; i++) {
      if (this.props.type[i] == 'sf') {
        data[this.props.apiNames[i]] = [this.state[this.props.apiNames[i] + "_selected"]][0]["id"];
      }
    }
    console.log("DATA TO BE ADDED", data);
    sendRequest(this, 'POST', this.props.url, this.state.dataToBeAdded);
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  saveChanges() {
    let pl = {};
    for (let i = 0; i < this.props.apiNames.length; i++) {
      if (this.props.type[i] == 'sf') {
        pl[this.props.apiNames[i]] = [this.state[this.props.apiNames[i] + "_selected"]][0]["id"];
      } else {
        pl[this.props.apiNames[i]] = this.state.dataToBeUpdated[this.props.apiNames[i]];
      }
    }

    console.log("PAYLOAD", pl);

    let fullUrl = this.props.url + "/" + this.state.dataToBeUpdated["id"];
    sendRequest(this, 'PUT', fullUrl, pl);
    this.setState({dataToBeUpdated: ""});
    this.setState({openDialog: false});
    this.setState({fcData: DATA_NOT_DOWNLOADED});
  }

  render() {
    return (
      <div ref={node => (this.root = node)}>
        <Dialog open={this.state.openDialog} className={s.dialog}>
          <DialogContent>
            <UltimateDataAdd {...this.props}
                             {...this.state}
                             key="UDA1"
                             mode="UPDATE"
                             buttonLabel=""
                             data={this.state.dataToBeUpdated}
                             updateParentState={this.updateParentState}
                             updateParentSelectState={this.updateParentSelectState}

            />
          </DialogContent>
          <DialogActions>
            <Button type='button' onClick={this.saveChanges}>Save Changes</Button>
            <Button type='button' onClick={this.handleCloseDialog}>Cancel</Button>
          </DialogActions>
        </Dialog>

        <Dialog open={this.state.openErrorDialog} className={s.dialog}>
          <DialogContent>
            There has been an error!
            <br/>
            {JSON.stringify(this.state.error)}
          </DialogContent>
          <DialogActions>
            <Button type='button' onClick={this.handleCloseErrorDialog}>Close</Button>
          </DialogActions>
        </Dialog>

        <Grid>
          <Cell col={4}>
            <div>
              <UltimateDataAdd {...this.props}
                               {...this.state}
                               key="UDA2"
                               mode="ADD"
                               data={this.state.dataToBeAdded}
                               updateParentState={this.updateParentState}
                               updateParentSelectState={this.updateParentSelectState}
                               addButtonOnClick={this.addData}
              />
            </div>
          </Cell>
          <Cell col={8}>
            <SpinnerList {...this.props}
                         getDataToBeUpdated={this.getDataToBeUpdated}
                         data={this.state.fcData}
                         onDelete={this.deleteWithId}
                         key={"FC" + String(this.props.titleKeys)}
                         editableDataList={true}
            />
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default ReservationContainer;
