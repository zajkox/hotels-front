/**
 * Created by atticus on 31.12.16.
 */
import React, {PropTypes} from 'react';
import DataContainer from '../DataContainter';
import VacancyContainer from '../VacancyContainter';
import ReservationContainer from '../ReservationContainter';
import {Grid, Cell} from 'react-mdl';


class Switcher extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    switch (this.props.activeTabName) {
      case "WELCOME":
        return (
          <div {...this.props} className={this.props.className}/>
        );
        break;
      case "HOTELS":
        break;
      case "RESERVATIONS":
        return (
          <div>
            <ReservationContainer
              label="Reservation Data"
              buttonLabel="Add Reservation"
              fieldNames={["Room ID", "Guest ID", "Start Date", "End Date"]}
              apiNames={["room_id", "guest_id", "start_date", "end_date"]}
              type={['sf', 'sf', 'df', 'df']}
              url="/reservations"
              sfUrl={["/rooms", "/guests"]}
              sfNameFields={[["room_number"],["first_name", "last_name"]]}
              titleKeys={["room_id"]}
            />
          </div>
        );
        break;
        break;
      case "ABOUT":
        return (
          <div>
            ABOUT
          </div>
        );
        break;
      case "ROOMS":
        return (
          <div>
            <DataContainer
              key="DCROOM"
              label="Room Data"
              buttonLabel="Add Room"
              fieldNames={["Room Number", "Room Capacity"]}
              apiNames={["room_number", "guests_amount"]}
              type={['tf', 'tf']}
              url="/rooms"
              titleKeys={["room_number"]}
            />
          </div>
        );
        break;
      case "GUESTS":
        return (
          <div>
            <DataContainer
              key="DCGUESTS"
              label="Guest Data"
              buttonLabel="Add Guest"
              fieldNames={["First Name", "Last Name", "Phone Number", "Guest ID"]}
              apiNames={["first_name", "last_name", "phone", "custom_id"]}
              type={['tf', 'tf', 'tf', 'tf']}
              url="/guests"
              titleKeys={["first_name", "last_name"]}
            />
          </div>
        );
        break;
      case "VACANT":
        return (
          <div>
            <VacancyContainer
              key="VACANT"
              label="Vacant Rooms"
              buttonLabel="Search"
              fieldNames={["Start Date", "End Date"]}
              apiNames={["start_date", "end_date"]}
              type={['df', 'df']}
              url="/rooms"
              titleKeys={["room_number"]}
              params={["start_date", "end_date"]}
            />
          </div>
        );
        break;
      default:
        return (
          <div>
            default
          </div>
        )

    }
  }
}

export default Switcher;
